package dev.codenation.app.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import dev.codenation.app.domain.Answer;
import dev.codenation.app.reponse.AnswerResponse;
import lombok.RequiredArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static dev.codenation.app.util.ConstantUtils.ALPHABET;

/**
 * @author : wesleyosantos91
 * @Date : 09/05/20
 * @Contact : wesleyosantos91@gmail.com
 **/
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AnswerService {

    private final RestTemplate restTemplate;

    @Value("${token}")
    private String token;

    @Value("${endpoint-codenative.dev}")
    private String endpoint;

    public Answer genereteData() {
        return restTemplate.getForEntity(endpoint + "/generate-data?token=" + token, Answer.class).getBody();
    }


    public AnswerResponse submitSolution() throws Exception {

        Answer load = genereteData();
        String decodeAnswer = decodeAnswer(load);
        String sha1 = generetSha1(decodeAnswer);

        load.setDecrypted(decodeAnswer);
        load.setAbstractCrypted(sha1);

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.writeValueAsString(load);

        return sendSubmitAsnswer(load, mapper);

    }

    private AnswerResponse sendSubmitAsnswer(Answer load, ObjectMapper mapper) throws IOException {

        File file = new File("answer.json");
        file.createNewFile();

        FileOutputStream fileOutputStream = new FileOutputStream(file);
        mapper.writeValue(fileOutputStream, load);
        fileOutputStream.close();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("answer", new FileSystemResource(file));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);

        return restTemplate.postForEntity(endpoint + "/submit-solution?token=" + token, requestEntity, AnswerResponse.class).getBody();

    }

    public String decodeAnswer(Answer answer) {

        char[] message = answer.getEncrypted().toLowerCase().toCharArray();
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < message.length; i++) {
            if (ALPHABET.contains(String.valueOf(message[i]))) {
                addDecodeCharecter(message[i], builder, answer);
            } else {
                builder.append(message[i]);
            }
        }

        return builder.toString();
    }

    public String encryptAnswer(Answer answer) {

        char[] message = answer.getDecrypted().toLowerCase().toCharArray();
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < message.length; i++) {
            if (ALPHABET.contains(String.valueOf(message[i]))) {
                addEncryptCharecter(message[i], builder, answer);
            } else {
                builder.append(message[i]);
            }
        }

        return builder.toString();
    }

    public String generetSha1(String encrypted) {
        return DigestUtils.sha1Hex(encrypted);
    }

    private void addDecodeCharecter(char c, StringBuilder builder, Answer answer) {

        char[] alphabet = ALPHABET.toCharArray();
        Integer numberOfHouse = answer.getNumberOfHouse();

        for (int i = 0; i < alphabet.length; i++) {
            if (i >= numberOfHouse && c == alphabet[i]) {
                builder.append(alphabet[i - numberOfHouse]);
                break;
            }

            if (c == alphabet[i] && i < numberOfHouse) {
                builder.append(alphabet[(alphabet.length - numberOfHouse) + i]);
                break;
            }
        }

    }

    private void addEncryptCharecter(char c, StringBuilder builder, Answer answer) {

        char[] alphabet = ALPHABET.toCharArray();
        Integer numberOfHouse = answer.getNumberOfHouse();

        for (int i = 0; i < alphabet.length; i++) {
            if (c == alphabet[i]) {
                builder.append(alphabet[(i + numberOfHouse) >= alphabet.length ? (i + numberOfHouse) - alphabet.length : i + numberOfHouse]);
                break;
            }
        }
    }
}
