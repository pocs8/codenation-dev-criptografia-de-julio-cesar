package dev.codenation.app.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author : wesleyosantos91
 * @Date : 09/05/20
 * @Contact : wesleyosantos91@gmail.com
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Answer {

    @JsonProperty("numero_casas")
    private Integer numberOfHouse;

    @JsonProperty("token")
    private String token;

    @JsonProperty("cifrado")
    private String encrypted;

    @JsonProperty("decifrado")
    private String decrypted;

    @JsonProperty("resumo_criptografico")
    private String abstractCrypted;
}
