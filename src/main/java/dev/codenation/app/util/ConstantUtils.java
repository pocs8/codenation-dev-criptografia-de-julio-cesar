package dev.codenation.app.util;

/**
 * @author : wesleyosantos91
 * @Date : 08/05/20
 * @Contact : wesleyosantos91@gmail.com
 **/
public class ConstantUtils {

    private ConstantUtils() {
    }

    public static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";

}
