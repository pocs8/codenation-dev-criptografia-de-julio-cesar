package dev.codenation.app.reponse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author : wesleyosantos91
 * @Date : 09/05/20
 * @Contact : wesleyosantos91@gmail.com
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AnswerResponse {

    private Long score;
}
