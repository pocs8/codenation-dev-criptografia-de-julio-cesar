package dev.codenation.app.resource;

import dev.codenation.app.domain.Answer;
import dev.codenation.app.reponse.AnswerResponse;
import dev.codenation.app.service.AnswerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : wesleyosantos91
 * @Date : 09/05/20
 * @Contact : wesleyosantos91@gmail.com
 **/
@RestController
@RequestMapping("/encryptions")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AnswerResource {

    private final AnswerService answerService;

    @GetMapping("/generate-data")
    public ResponseEntity<Answer> genereteData() {
        return ResponseEntity.ok(answerService.genereteData());
    }

    @PostMapping("/submit-solution")
    public ResponseEntity<AnswerResponse> submitSolution() throws Exception {
        return ResponseEntity.ok(answerService.submitSolution());
    }


}
